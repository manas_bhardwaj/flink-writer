import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.JsonNodeFactory;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.util.serialization.JSONKeyValueDeserializationSchema;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class flinkWriter {
    public static Logger LOGGER = LoggerFactory.getLogger(flinkWriter.class);

    public static void main(String[] args) throws Exception {
        ParameterTool parameterTool = ParameterTool.fromPropertiesFile("src/main/resources/schema.properties");
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        env.getConfig().setAutoWatermarkInterval(1000);
        env.enableCheckpointing(1000);
        CheckpointConfig config = env.getCheckpointConfig();
        config.enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setStateBackend(new FsStateBackend("file:///data/flink/payment/checkpoints"));
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", "kafka1.cleartrip.com:9092,kafka2.cleartrip.com:9092,kafka3.cleartrip.com:9092,kafka4.cleartrip.com:9092,kafka5.cleartrip.com:9092,kafka6.cleartrip.com:9092,kafka7.cleartrip.com:9092,kafka8.cleartrip.com:9092");
        props.setProperty("group.id", "mysql-flink-payment-job-1");
        props.setProperty("auto.offset.reset", "earliest");
//        props.setProperty("enable.auto.commit", "false");
        LOGGER.info("Property initialization completed");
        FlinkKafkaConsumer<ObjectNode> consumer = new FlinkKafkaConsumer<>("maxwell_payment", new JSONKeyValueDeserializationSchema(false), props);
        DataStream<ObjectNode> tuples = env.addSource(consumer);
        tuples.map((MapFunction<ObjectNode, ObjectNode>) value -> {
            ObjectNode node = new ObjectNode(JsonNodeFactory.instance);
            node.set(value.get("value").get("table").asText(),value.get("value").get("data"));
            return node;
        })
                .keyBy(node->node.fieldNames().next())
                .timeWindow(Time.seconds(10))
                .process(new ProcessWindowFunction<ObjectNode, Object, String, TimeWindow>() {
                    @Override
                    public void process(String tableName, Context context, Iterable<ObjectNode> elements, Collector<Object> out) throws Exception {
                        for(ObjectNode node : elements) {
//                            LOGGER.info("IN PROCESS FUNCTION : " + node);
                            writeToCSV(node);
                        }
                        String[] columns = parameterTool.get(tableName).split(",");
//                        LOGGER.info("In process function : Columns are : " + columns);
                        String filePath = "/home/manasbhardwaj/Personal-Workspace/Work/Flink-Writer/data/" + tableName + ".csv";
                        WriteJSONToCSV(elements,columns,filePath,tableName);
                    }
                });
        System.out.println(env.execute("Flink-kafka-csvWriter").getAllAccumulatorResults().toString());

    }
    public static void writeToCSV(ObjectNode obj) throws IOException {
//        LOGGER.info("Writing data to CSV");
        try {
            String csvFileName = "/home/manasbhardwaj/Personal-Workspace/Work/Flink-Writer/data/dump.csv";
            File file = new File(csvFileName);
            FileWriter fr = new FileWriter(file, true);
            fr.write(obj.toString());
            fr.write("\n\n\n");
            fr.close();
        } catch (Exception e) {
            LOGGER.error("Exception Occured in writeToCSV function " + e.toString() + "    " + e.getMessage());
        }

    }

    public static void WriteJSONToCSV(Iterable<ObjectNode> list, String[] cols, String filePath,String tableName) throws IOException {
        LOGGER.info("Writing data to WriteJSONToCSV");
        File file = new File(filePath);
        if(!file.exists()){
            LOGGER.info("File created and written the columns");
            file.createNewFile();
            FileWriter fr = new FileWriter(file,true);
            fr.write(String.join(",",cols));
            fr.write("\n");
            fr.close();
        }
        FileWriter fr = new FileWriter(file,true);
//        LOGGER.info("Written column of table : " + tableName + "  :  " + String.join(",",cols));
        for(ObjectNode node : list){
            LOGGER.info("OBJECTNODE TEXT : " + node);
            String[] temp = new String[cols.length];
            int count = 0;
            for(String col : cols){
                temp[count++] = String.valueOf(node.get(tableName).get(col));
            }
            String r = String.join(",",temp);
            LOGGER.info("WRITING ROW : " + r);
            fr.write(r);
            fr.write("\n");
        }
        fr.close();

    }
}
